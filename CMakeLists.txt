cmake_minimum_required(VERSION 3.1.0)
# cmake_minimum_required(VERSION 2.8)

project(aula-1 VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

find_package(Qt5 REQUIRED
  Core
  Qml
  Quick
  Quickcontrols2
  Widgets
)

add_subdirectory(src)